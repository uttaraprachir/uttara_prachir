package com.erica.controller;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/login")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String SUCCESS = "Success";
	public static final String FAILURE = "Login Failed.Please try again.";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("jsp/login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String propertiesFilePath = getServletContext().getRealPath("data.properties");
		FileReader fileReader = new FileReader(new File(propertiesFilePath));
		System.out.println(request.getParameter("Username"));
		System.out.println(request.getParameter("Password"));
		Properties prop = new Properties();
		prop.load(fileReader);

		String uname = prop.getProperty("data.username");
		String pass = prop.getProperty("data.password");
		System.out.println("uname :: " + uname);
		System.out.println("pass :: " + pass);

		if (request.getParameter("Username").equals(uname) && request.getParameter("Password").equals(pass)) {
			request.setAttribute("loginStatus", "");

			request.getSession().setAttribute("uname", uname);
		} else {
			request.setAttribute("loginStatus", FAILURE);
		}

		request.getRequestDispatcher("jsp/index.jsp").forward(request, response);
	}

}
