<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url var="home" value="home" scope="page"></c:url>
<c:url var="about" value="about" scope="page"></c:url>
<c:url var="sendEmail" value="sendEmail" scope="page"></c:url>

<div class="header-bottom">
	<div class="wrap">
		<nav id="menu-wrap">
			<ul id="menu">
				<li><a href="${home}">Home</a></li>
				<li><a href="${about}">About</a></li>
				<c:if test="${sessionScope.uname ne null}">
					<li><a href="${sendEmail}">Send Mail</a></li>
				</c:if>
			</ul>
		</nav>
	</div>
</div>