<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<c:url var="upload" value="home" scope="page"></c:url>

<jsp:include page="header.jsp"></jsp:include>
<jsp:include page="menu.jsp"></jsp:include>


<c:choose>
	<c:when test="${sessionScope.uname ne null}">
		<div class="main-cont">
			<div class="main">
				<div class="wrap">
					<div class="content-grids">
						<div class="content-grid">
							<h3>ADMIN PAGE CONTENT</h3>
							 
							<form action="${upload}" method="post" id="member1"
								autocomplete="off">
								<table>
									<tr>
										<td>
											<p>From Date</p>
										</td>
										<td>
											<p>
												<input name="fromDate" type="date" class="field">
											</p>
										</td>
										<td>
											<p>To Date</p>
										</td>
										<td>
											<p>
												<input type="date" name="toDate" class="field">
											</p>
										</td>
										<td><input type="submit" name="submit" class="btn" style="    margin: -20px 13px;"
											value="Search" /></td>
									</tr>
								</table>
							</form>
							<display:table id="data" name="${garbageCollection}"
								varTotals="garbage" export="true" class="garbageTable"
								requestURI="${upload}" pagesize="6">
								<display:column property="grCode" title="Garbage Code"
									sortable="true" />
								<display:column property="locality" title="Locality"
									sortable="true" />
								<display:column property="wasteType" title="Waste Type"
									sortable="true" />
								<display:column property="uploadDate" title="Upload Date"
									sortable="true" />
								<display:column title="Download Image" property="filePath">

								</display:column>
								<display:setProperty name="export.excel.filename"
									value="GarbageDetails.xls" />
							</display:table>
							 
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:when>
	<c:otherwise>
		<div class="main-cont">
			<div class="main">
				<div class="wrap">
					<div class="content-grids">
						<div class="content-grid">
							<h3>USER PAGE CONTENT</h3>
							 
							<form action="${upload}" method="post" id="member"
								enctype="multipart/form-data" autocomplete="off">
								<table>
									<tr>
										<td>
											<p>Locality</p>
										</td>
										<td>
											<p>
												<input name="Locality" type="text" class="field">
											</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>Waste Type</p>
										</td>
										<td>
											<p>
												<select name="Type" class="field">
													<option value="Organic">Organic</option>
													<option value="In-Organic">In-Organic</option>
												</select>
											</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>Upload Image</p>
										</td>
										<td>
											<p>
												<input type="file" name="file" class="field">
											</p>
										</td>
									</tr>
								</table>
								<input type="submit" name="submit" class="btn" style="float: left;"
									value="Upload" />
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>



<jsp:include page="footer.jsp"></jsp:include>