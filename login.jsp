<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:url var="login" value="login" scope="page"></c:url>

<jsp:include page="header.jsp"></jsp:include>
<jsp:include page="menu.jsp"></jsp:include>

<div class="main-cont">
	<div class="main no_main">
		<div class="wrap">
			<div class="about-grids">
				<div class="login_panel">
					<h3>Admin Login</h3>
					<p>Sign in with the form below.</p>
					<form action="${login}" method="post" id="member">
						<input name="Username" type="text" value="Username" class="field"
							onfocus="this.value = '';"
							onblur="if (this.value == '') {this.value = 'Username';}">
						<input name="Password" type="password" value="Password"
							class="field" onfocus="this.value = '';"
							onblur="if (this.value == '') {this.value = 'Password';}">
						<input type="submit" name="submit"  class="btn" style="float: left;" value="Sign In" />
					</form>


				</div>

				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

<jsp:include page="footer.jsp"></jsp:include>